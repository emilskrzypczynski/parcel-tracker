# Parcel tracker 

*Project under development

## Requirements
1. `docker` and `docker-compose`

## Installation
1. Download project

   `git clone https://gitlab.com/emilskrzypczynski/parcel-tracker.git`

2. Go to project dir

   `cd parcel-tracker`

3. Create `.env` file from `.env.dist`. Change parameters in created file.

4. Run `docker-compose build` and `docker-compose up -d`

5. Open browser on `http://localhost`
