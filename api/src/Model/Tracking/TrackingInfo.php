<?php


namespace App\Model\Tracking;


use App\Model\Traits\IdTrait;
use App\Model\Traits\TimestampableTrait;
use App\Model\Traits\UpdatableTrait;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;

class TrackingInfo
{
    use IdTrait,
        TimestampableTrait,
        UpdatableTrait;

    public const CACHE_TIME = '5 minutes';

    private string $trackingNumber;
    private bool $success = false;
    private ?string $originCountry = null;
    private ?string $destinationCountry = null;
    private ?int $status = null;
    private ?string $statusDesc = null;
    private ?\DateTime $createdDate = null;
    private ?string $carrier = null;
    private ?string $consolidationTrackingNumber = null;
    private ?int $shippingTime = null;
    private Collection $events;
    private ?TrackingEvent $lastEvent = null;

    public function __construct()
    {
        $this->events = new ArrayCollection();
        $this->createdAt = new \DateTime();
    }

    public function isFresh(): bool
    {
        $lastModified = $this->updatedAt ?? $this->createdAt;

        return new \DateTime() <= ($lastModified->modify(sprintf('+ %s', self::CACHE_TIME))) ;
    }

    public function getTrackingNumber(): ?string
    {
        return $this->trackingNumber;
    }

    public function setTrackingNumber(string $trackingNumber): self
    {
        $this->trackingNumber = $trackingNumber;

        return $this;
    }

    public function getSuccess(): ?bool
    {
        return $this->success;
    }

    public function setSuccess(bool $success): self
    {
        $this->success = $success;

        return $this;
    }

    public function getOriginCountry(): ?string
    {
        return $this->originCountry;
    }

    public function setOriginCountry(?string $originCountry): self
    {
        $this->originCountry = $originCountry;

        return $this;
    }

    public function getDestinationCountry(): ?string
    {
        return $this->destinationCountry;
    }

    public function setDestinationCountry(?string $destinationCountry): self
    {
        $this->destinationCountry = $destinationCountry;

        return $this;
    }

    public function getStatus(): ?string
    {
        return $this->status;
    }

    public function setStatus(?string $status): self
    {
        $this->status = $status;

        return $this;
    }

    public function getStatusDesc(): ?string
    {
        return $this->statusDesc;
    }

    public function setStatusDesc(?string $statusDesc): self
    {
        $this->statusDesc = $statusDesc;

        return $this;
    }

    public function getCreatedDate(): ?\DateTimeInterface
    {
        return $this->createdDate;
    }

    public function setCreatedDate(?\DateTimeInterface $createdDate): self
    {
        $this->createdDate = $createdDate;

        return $this;
    }

    public function getCarrier(): ?string
    {
        return $this->carrier;
    }

    public function setCarrier(?string $carrier): self
    {
        $this->carrier = $carrier;

        return $this;
    }

    public function getConsolidationTrackingNumber(): ?string
    {
        return $this->consolidationTrackingNumber;
    }

    public function setConsolidationTrackingNumber(?string $consolidationTrackingNumber): self
    {
        $this->consolidationTrackingNumber = $consolidationTrackingNumber;

        return $this;
    }

    public function getShippingTime(): ?int
    {
        return $this->shippingTime;
    }

    public function setShippingTime(?int $shippingTime): self
    {
        $this->shippingTime = $shippingTime;

        return $this;
    }

    public function getLastEvent(): ?TrackingEvent
    {
        return $this->lastEvent;
    }

    public function setLastEvent(?TrackingEvent $lastEvent): self
    {
        $this->lastEvent = $lastEvent;

        return $this;
    }

    /**
     * @return Collection|TrackingEvent[]
     */
    public function getEvents(): Collection
    {
        return $this->events;
    }

    public function addEvent(TrackingEvent $event): self
    {
        if (!$this->events->contains($event)) {
            $this->events[] = $event;
            $event->setTrackingInfo($this);
        }

        return $this;
    }

    public function removeEvent(TrackingEvent $event): self
    {
        if ($this->events->removeElement($event)) {
            // set the owning side to null (unless already changed)
            if ($event->getTrackingInfo() === $this) {
                $event->setTrackingInfo(null);
            }
        }

        return $this;
    }
}
