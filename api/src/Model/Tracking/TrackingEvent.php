<?php


namespace App\Model\Tracking;


use App\Model\Traits\IdTrait;
use App\Model\Traits\TimestampableTrait;
use App\Model\Traits\UpdatableTrait;

class TrackingEvent
{
    use IdTrait,
        TimestampableTrait,
        UpdatableTrait;

    protected TrackingInfo $trackingInfo;
    protected string $name;
    protected string $carrier;
    protected ?string $code = null;
    protected ?string $status = null;
    protected ?string $reason = null;
    protected ?string $branchName = null;
    protected ?string $branchAddress = null;
    protected ?string $branchComment = null;
    protected ?\DateTimeInterface $createdDate = null;

    public function __construct()
    {
        $this->createdAt = new \DateTime();
    }


    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getCarrier(): ?string
    {
        return $this->carrier;
    }

    public function setCarrier(string $carrier): self
    {
        $this->carrier = $carrier;

        return $this;
    }

    public function getCreatedDate(): ?\DateTimeInterface
    {
        return $this->createdDate;
    }

    public function setCreatedDate(?\DateTimeInterface $createdDate): self
    {
        $this->createdDate = $createdDate;

        return $this;
    }

    public function getCode(): ?string
    {
        return $this->code;
    }

    public function setCode(?string $code): self
    {
        $this->code = $code;

        return $this;
    }

    public function getStatus(): ?string
    {
        return $this->status;
    }

    public function setStatus(?string $status): self
    {
        $this->status = $status;

        return $this;
    }

    public function getReason(): ?string
    {
        return $this->reason;
    }

    public function setReason(?string $reason): self
    {
        $this->reason = $reason;

        return $this;
    }

    public function getBranchName(): ?string
    {
        return $this->branchName;
    }

    public function setBranchName(?string $branchName): self
    {
        $this->branchName = $branchName;

        return $this;
    }

    public function getBranchAddress(): ?string
    {
        return $this->branchAddress;
    }

    public function setBranchAddress(?string $branchAddress): self
    {
        $this->branchAddress = $branchAddress;

        return $this;
    }

    public function getBranchComment(): ?string
    {
        return $this->branchComment;
    }

    public function setBranchComment(?string $branchComment): self
    {
        $this->branchComment = $branchComment;

        return $this;
    }

    public function getTrackingInfo(): ?TrackingInfo
    {
        return $this->trackingInfo;
    }

    public function setTrackingInfo(TrackingInfo $trackingInfo): self
    {
        $this->trackingInfo = $trackingInfo;

        return $this;
    }
}
