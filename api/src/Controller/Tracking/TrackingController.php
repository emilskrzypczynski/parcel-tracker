<?php


namespace App\Controller\Tracking;


use App\Command\Tracking\GetTrackingInfo;
use App\Controller\RestControllerTrait;
use App\Utils\Tracking\Carriers;
use App\Utils\Tracking\Exception\TrackingException;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\View\View;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Messenger\HandleTrait;
use Symfony\Component\Messenger\MessageBusInterface;

class TrackingController extends AbstractFOSRestController
{
    use HandleTrait,
        RestControllerTrait;

    public function __construct(
        MessageBusInterface $messageBus
    )
    {
        $this->messageBus = $messageBus;
    }

    /**
     * @Rest\Get(path="/tracking/{number}")
     * @Rest\View(serializerGroups={
     *     "Default",
     *     "TrackingInfoDetails"
     * })
     */
    public function track(string $number)
    {
        try {
            $info = $this->handle(
                new GetTrackingInfo(
                    $number,
                    [
                        Carriers::POCZTA_POLSKA,
                        Carriers::CAINIAO,
                        Carriers::DHL
                    ]
                )
            );

            if (null === $info) {
                throw new TrackingException("Not found", 404);
            }

            return View::create(
                $info
            );
        } catch (TrackingException $e) {
            return View::create([
                'message' => 'Not found',
            ], Response::HTTP_NOT_FOUND);
        }
    }
}
