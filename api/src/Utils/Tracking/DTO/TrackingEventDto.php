<?php


namespace App\Utils\Tracking\DTO;


class TrackingEventDto
{
    protected string $name;
    protected string $carrier;
    protected ?\DateTimeInterface $createdDate;
    protected ?string $code;
    protected ?string $status;
    protected ?string $reason;
    protected ?string $branchName;
    protected ?string $branchAddress;
    protected ?string $branchComment;

    public function __construct(
        string $name,
        string $carrier,
        ?\DateTimeInterface $createdDate = null,
        ?string $code = null,
        ?string $status = null,
        ?string $reason = null,
        ?string $branchName = null,
        ?string $branchAddress = null,
        ?string $branchComment = null
    ) {
        $this->name = $name;
        $this->carrier = $carrier;
        $this->createdDate = $createdDate;
        $this->code = $code;
        $this->status = $status;
        $this->reason = $reason;
        $this->branchName = $branchName;
        $this->branchAddress = $branchAddress;
        $this->branchComment = $branchComment;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getCarrier(): string
    {
        return $this->carrier;
    }

    public function getCreatedDate(): ?\DateTimeInterface
    {
        return $this->createdDate;
    }

    public function getCode(): ?string
    {
        return $this->code;
    }

    public function getStatus(): ?string
    {
        return $this->status;
    }

    public function getReason(): ?string
    {
        return $this->reason;
    }

    public function getBranchName(): ?string
    {
        return $this->branchName;
    }

    public function getBranchAddress(): ?string
    {
        return $this->branchAddress;
    }

    public function getBranchComment(): ?string
    {
        return $this->branchComment;
    }
}
