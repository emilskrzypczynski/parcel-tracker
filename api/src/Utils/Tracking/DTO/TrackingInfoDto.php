<?php


namespace App\Utils\Tracking\DTO;


use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;

class TrackingInfoDto
{
    private string $trackingNumber;
    private bool $success;
    private ?\DateTime $createdDate;
    private ?string $originCountry;
    private ?string $destinationCountry;
    private ?int $status;
    private ?string $statusDesc;
    private ?string $carrier;
    private ?string $consolidationTrackingNumber;
    private ?int $shippingTime;
    private ?array $events;
    private ?TrackingEventDto $lastEvent;

    public function __construct(
        string $trackingNumber,
        bool $success,
        ?\DateTime $createdDate = null,
        ?string $originCountry = null,
        ?string $destinationCountry = null,
        ?int $status = null,
        ?string $statusDesc = null,
        ?string $carrier = null,
        ?string $consolidationTrackingNumber = null,
        ?int $shippingTime = null,
        ?array $events = null,
        ?TrackingEventDto $lastEvent = null
    )
    {
        $this->trackingNumber = $trackingNumber;
        $this->success = $success;
        $this->originCountry = $originCountry;
        $this->destinationCountry = $destinationCountry;
        $this->status = $status;
        $this->statusDesc = $statusDesc;
        $this->createdDate = $createdDate;
        $this->carrier = $carrier;
        $this->consolidationTrackingNumber = $consolidationTrackingNumber;
        $this->shippingTime = $shippingTime;
        $this->events = $events;
        $this->lastEvent = $lastEvent;
    }

    public function getTrackingNumber(): string
    {
        return $this->trackingNumber;
    }

    public function isSuccess(): bool
    {
        return $this->success;
    }

    public function getCreatedDate(): ?\DateTime
    {
        return $this->createdDate;
    }

    public function getOriginCountry(): ?string
    {
        return $this->originCountry;
    }

    public function getDestinationCountry(): ?string
    {
        return $this->destinationCountry;
    }

    public function getStatus(): ?int
    {
        return $this->status;
    }

    public function getStatusDesc(): ?string
    {
        return $this->statusDesc;
    }

    public function getCarrier(): ?string
    {
        return $this->carrier;
    }

    public function getConsolidationTrackingNumber(): ?string
    {
        return $this->consolidationTrackingNumber;
    }

    public function getShippingTime(): ?int
    {
        return $this->shippingTime;
    }

    public function getEvents(): ?array
    {
        return $this->events;
    }

    public function setEvents(?array $events): TrackingInfoDto
    {
        $this->events = $events;
        return $this;
    }

    public function getLastEvent(): ?TrackingEventDto
    {
        return $this->lastEvent;
    }
}
