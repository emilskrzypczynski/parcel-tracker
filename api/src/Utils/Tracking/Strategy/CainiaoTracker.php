<?php


namespace App\Utils\Tracking\Strategy;

use App\Utils\Tracking\Carriers;
use App\Utils\Tracking\DTO\TrackingEventDto;
use App\Utils\Tracking\DTO\TrackingInfoDto;
use Symfony\Component\DomCrawler\Crawler;
use Symfony\Contracts\HttpClient\HttpClientInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

class CainiaoTracker implements TrackerInterface
{
    private const API_URL = 'https://global.cainiao.com/detail.htm';

    private HttpClientInterface $httpClient;
    private TranslatorInterface $translator;

    public function __construct(
        HttpClientInterface $httpClient,
        TranslatorInterface $translator
    )
    {
        $this->httpClient = $httpClient;
        $this->translator = $translator;
    }

    public function track(string $number): ?TrackingInfoDto
    {
        $request = $this->httpClient->request(
            'GET',
            sprintf("%s?mailNoList=%s", self::API_URL, $number)
        );

        $content = $request->getContent();
        $crawler = new Crawler($content);

        $json = json_decode($crawler->filter('#waybill_list_val_box')->html(), true);

        if (!isset($json['data'][0]['destCountry']) || !isset($json['data'][0]['originCountry'])) {
            return null;
        }

        $data = $json['data'][0];

        $events = [];

        foreach ($data['section1']['detailList'] as $originEvent) {
            $events[] = $this->createEvent($originEvent);
        }

        foreach ($data['section2']['detailList'] as $destEvent) {
            $events[] = $this->createEvent($destEvent);
        }

        if (preg_match('/^([a-zA-Z0-9]+)/', $data['mailNo'], $matches)) {
            $trackingNumber = $matches[0];
        } else {
            $trackingNumber = $data['mailNo'];
        }

        return new TrackingInfoDto(
            $trackingNumber,
            $data['success'],
            null,
            $data['originCountry'],
            $data['destCountry'],
            null,
            null,
            Carriers::CAINIAO,
            $data['section2']['mailNo'] ?? $data['section1']['mailNo'] ?? null,
            $data['shippingTime'] ?? null,
            $events,
            !empty($events) ? $events[0] : null
        );
    }

    private function createEvent(array $details): TrackingEventDto
    {
        $createdAt = !empty($details['time']) ?
            new \DateTime($details['time'], new \DateTimeZone(!empty($details['timeZone']) ? $details['timeZone'] : '+1')) :
            null;

        $desc = $details['desc'];

        // Extract some values from desc
        if (preg_match('/^-(.*)-/', $details['desc'], $matches)) {
            $branchName = $matches[1];

            $desc = preg_replace('/^-(.*)-/', '', $details['desc']);
        }

        // Find description translation (status first, next description field)
        $statusDesc = $this->translator->trans(
            $details['status'],
            [],
            'tracking_cainiao'
        );

        if (!empty($statusDesc) && $statusDesc !== $details['status']) {
            $desc = $this->translator->trans(
                $statusDesc,
                [],
                'tracking_cainiao'
            );
        } else {
            $desc = $this->translator->trans(
                $desc,
                [],
                'tracking_cainiao'
            );
        }

        return new TrackingEventDto(
            $desc,
            Carriers::CAINIAO,
            $createdAt,
            null,
            $details['status'],
            null,
            $branchName ?? null,
            null,
            null
        );
    }

    public function getName(): string
    {
        return Carriers::CAINIAO;
    }
}
