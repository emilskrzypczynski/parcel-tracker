<?php


namespace App\Utils\Tracking\Strategy;


use App\Utils\Tracking\DTO\TrackingInfoDto;

interface TrackerInterface
{
    public function getName(): string;
    public function track(string $number): ?TrackingInfoDto;
}
