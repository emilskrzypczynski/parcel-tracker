<?php


namespace App\Utils\Tracking\Strategy;

use App\Utils\Soap\WsseAuthHeader;
use App\Utils\Tracking\Carriers;
use App\Utils\Tracking\DTO\TrackingEventDto;
use App\Utils\Tracking\DTO\TrackingInfoDto;

class PocztaPolskaTracker implements TrackerInterface
{
    private string $wsUrl;
    private string $wsUsername;
    private string $wsPassword;

    public function __construct(
        string $wsUrl,
        string $wsUsername,
        string $wsPassword
    )
    {
        $this->wsUrl = $wsUrl;
        $this->wsUsername = $wsUsername;
        $this->wsPassword = $wsPassword;
    }

    public function track(string $number): ?TrackingInfoDto
    {
        $client = new \SoapClient(
            'https://tt.poczta-polska.pl/Sledzenie/services/Sledzenie?wsdl',
            [
                'location' => $this->wsUrl,
                'uri' => $this->wsUrl,
                'cache_wsdl' => 0,
                'trace' => 1
            ]
        );

        $client->__setSoapHeaders([
            new WsseAuthHeader($this->wsUsername, $this->wsPassword)
        ]);

        $data = $client->sprawdzPrzesylkePl([
            'numer' => $number
        ]);

        if (empty($data->return) || !in_array($data->return->status, [0, 1]) || empty($data->return->danePrzesylki)) {
            return null;
        }

        $data = $data->return;
        $packageInfo = $data->danePrzesylki;



        $events = [];

        foreach (array_reverse($packageInfo->zdarzenia->zdarzenie) as $event) {
            $branchAddress = [
                $event->jednostka->daneSzczegolowe->pna ?? null,
                $event->jednostka->daneSzczegolowe->miejscowosc ? $event->jednostka->daneSzczegolowe->miejscowosc . ',': null,
                $event->jednostka->daneSzczegolowe->ulica ?? null,
                $event->jednostka->daneSzczegolowe->nrDomu ?? null,
                $event->jednostka->daneSzczegolowe->nrLokalu ? '/' . $event->jednostka->daneSzczegolowe->nrLokalu : null,
            ];

            $branchComment = [
                'pon-pt' => [
                    'hours' => $event->jednostka->daneSzczegolowe->godzinyPracy->dniRobocze->godziny ?? null,
                    'comment' => $event->jednostka->daneSzczegolowe->godzinyPracy->dniRobocze->uwagi ?? null
                ],
                'sob' => [
                    'hours' => $event->jednostka->daneSzczegolowe->godzinyPracy->soboty->godziny ?? null,
                    'comment' => $event->jednostka->daneSzczegolowe->godzinyPracy->soboty->uwagi ?? null
                ],
                'nd' => [
                    'hours' => $event->jednostka->daneSzczegolowe->godzinyPracy->niedzISw->godziny ?? null,
                    'comment' => $event->jednostka->daneSzczegolowe->godzinyPracy->niedzISw->uwagi ?? null
                ],
            ];

            $trackingEvent = new TrackingEventDto(
                $event->nazwa,
                Carriers::POCZTA_POLSKA,
                $event->czas ? \DateTime::createFromFormat('Y-m-d H:i', $event->czas) : null,
                $event->kod,
                null,
                $event->przyczyna ? $event->przyczyna->nazwa : null,
                $event->jednostka->nazwa,
                implode(" ", array_filter($branchAddress)),
                null
            );

            $events[] = $trackingEvent;
        }

        return new TrackingInfoDto(
            $packageInfo->numer,
            $packageInfo->zakonczonoObsluge ?? false,
            $data->danePrzesylki->dataNadania ?
                \DateTime::createFromFormat('Y-m-d', $data->danePrzesylki->dataNadania) :
                null,
            $packageInfo->krajNadania ?? null,
            $packageInfo->krajPrzezn ?? null,
            $data->status,
            null,
            Carriers::POCZTA_POLSKA,
            null,
            null,
            $events,
            !empty($events) ? $events[0]: null
        );
    }

    public function getName(): string
    {
        return Carriers::POCZTA_POLSKA;
    }
}
