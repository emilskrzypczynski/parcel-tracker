<?php


namespace App\Utils\Tracking\Strategy;


use App\Utils\Tracking\Carriers;
use App\Utils\Tracking\DTO\TrackingEventDto;
use App\Utils\Tracking\DTO\TrackingInfoDto;
use Symfony\Contracts\HttpClient\HttpClientInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

class DHLTracker implements TrackerInterface
{
    private const API_URL = 'https://api-eu.dhl.com/track/shipments';

    private HttpClientInterface $httpClient;
    private TranslatorInterface $translator;
    private string $apiKey;

    public function __construct(
        HttpClientInterface $httpClient,
        TranslatorInterface $translator,
        string $apiKey
    )
    {
        $this->httpClient = $httpClient;
        $this->translator = $translator;
        $this->apiKey = $apiKey;
    }

    public function track(string $number): ?TrackingInfoDto
    {
        $request = $this->httpClient->request(
            'GET',
            sprintf("%s?trackingNumber=%s", self::API_URL, $number),
            [
                'headers' => [
                    'Content-Type' => 'application/json',
                    'DHL-API-Key' => $this->apiKey
                ],
            ]
        );

        if (200 !== $request->getStatusCode()) {
            return null;
        }

        $content = $request->getContent();

        $json = json_decode($content, true);

        $lastShipment = end($json['shipments']);

        $events = [];

        foreach ($lastShipment['events'] as $event) {
            $translationKey = $event['status'] ?? $event['statusCode'] ?? $event['description'] ?? null;

            $eventName = $translationKey ?
                $this->translator->trans($translationKey, [], 'tracking_dhl') :
                null;


            $events[] = new TrackingEventDto(
                $eventName,
                Carriers::DHL,
                isset($event['timestamp']) && null !== $event['timestamp'] ?
                    new \DateTime($event['timestamp']) :
                    null,
                null,
                null,
                null,
                isset($event['location']) ? $event['location']['address']['addressLocality']: null,
            );
        }

        return new TrackingInfoDto(
            $lastShipment['id'],
            false,
            null,
            $lastShipment['origin']['address']['addressLocality'] ?? null,
            $lastShipment['destination']['address']['addressLocality'] ?? null,
            null,
            $lastShipment['status']['description'] ?? null,
            Carriers::DHL,
            null,
            null,
            $events,
            !empty($events) ? $events[0] : null
        );
    }

    public function getName(): string
    {
        return Carriers::DHL;
    }
}
