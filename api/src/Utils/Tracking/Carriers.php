<?php


namespace App\Utils\Tracking;


class Carriers
{
    public const CAINIAO = 'Cainiao';
    public const POCZTA_POLSKA = 'Poczta Polska';
    public const DHL = 'DHL';
    public const INPOST = 'InPost';
}
