<?php


namespace App\Utils\Tracking;


use App\Utils\Tracking\Exception\TrackingException;
use App\Utils\Tracking\DTO\TrackingEventDto;
use App\Utils\Tracking\Strategy\TrackerInterface;
use App\Utils\Tracking\DTO\TrackingInfoDto;
use Doctrine\Common\Collections\ArrayCollection;

class Tracking
{
    private array $strategies;

    public function __construct(array $strategies)
    {
        $this->strategies = $strategies;
    }

    public function addStrategy(TrackerInterface $strategy): void
    {
        $this->strategies[] = $strategy;
    }

    public function getInfo(string $number): ?TrackingInfoDto
    {
        if (0 === count($this->strategies)) {
            throw new TrackingException("No tracking strategies defined.", 0);
        }

        $trackingData = [];

        /** @var TrackerInterface $strategy */
        foreach ($this->strategies as $key => $strategy) {
            $trackingData[$key] = $strategy->track($number);
        }

        $availableTrackings = array_filter($trackingData);

        // No trackings available
        if (0 === count($availableTrackings)) {
            return null;
        }

        /** @var TrackingInfoDto $baseTrackingInfo */
        $baseTrackingInfo = $availableTrackings[array_key_first($availableTrackings)];

        // Gather events from each tracking
        $allEvents = [];

        /** @var TrackingInfoDto $trackingInfo */
        foreach ($availableTrackings as $trackingInfo) {
            foreach ($trackingInfo->getEvents() as $event) {
                $allEvents[] = $event;
            }
        }

        // No events - null response
        if (0 === count($allEvents)) {
            return null;
        }

        // Order events by date desc
        usort($allEvents, function (TrackingEventDto $event1, TrackingEventDto $event2) {
           return $event1->getCreatedDate() < $event2->getCreatedDate();
        });

        $baseTrackingInfo->setEvents($allEvents);

        return $baseTrackingInfo;
    }
}
