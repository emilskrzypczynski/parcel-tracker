<?php


namespace App\Utils\Tracking\Exception;


use Throwable;

class TrackingException extends \Exception
{
    public function __construct(string $message = "", int $code = 0, Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}
