<?php


namespace App\Command\Tracking;


class GetTrackingInfo
{
    private string $number;
    private array $carriers;

    public function __construct(
        string $number,
        array $carriers
    ) {
        $this->number = $number;
        $this->carriers = $carriers;
    }

    public function getNumber(): string
    {
        return $this->number;
    }

    public function getCarriers(): array
    {
        return $this->carriers;
    }
}
