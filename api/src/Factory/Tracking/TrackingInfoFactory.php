<?php


namespace App\Factory\Tracking;


use App\Model\Tracking\TrackingEvent;
use App\Model\Tracking\TrackingInfo;
use App\Utils\Tracking\DTO\TrackingEventDto;
use App\Utils\Tracking\DTO\TrackingInfoDto;

class TrackingInfoFactory
{
    public static function fromDto(TrackingInfoDto $trackingInfoDto, TrackingInfo $trackingInfo = null): TrackingInfo
    {
        $updatedAt = $trackingInfo ? new \DateTime() : null;

        $trackingInfo = $trackingInfo ?? new TrackingInfo();

        $trackingInfo
            ->setTrackingNumber($trackingInfoDto->getTrackingNumber())
            ->setUpdatedAt($updatedAt)
            ->setSuccess($trackingInfoDto->isSuccess())
            ->setOriginCountry($trackingInfoDto->getOriginCountry())
            ->setDestinationCountry($trackingInfoDto->getDestinationCountry())
            ->setStatus($trackingInfoDto->getStatus())
            ->setStatusDesc($trackingInfoDto->getStatusDesc())
            ->setCreatedDate($trackingInfoDto->getCreatedDate())
            ->setCarrier($trackingInfoDto->getCarrier())
            ->setConsolidationTrackingNumber($trackingInfoDto->getConsolidationTrackingNumber())
            ->setShippingTime($trackingInfoDto->getShippingTime());

        $trackingInfo->getEvents()->clear();

        /** @var TrackingEventDto $trackingEventDto */
        foreach ($trackingInfoDto->getEvents() as $trackingEventDto) {
            $trackingEvent = new TrackingEvent();

            $trackingEvent
                ->setName($trackingEventDto->getName())
                ->setCarrier($trackingEventDto->getCarrier())
                ->setCreatedDate($trackingEventDto->getCreatedDate())
                ->setCode($trackingEventDto->getCode())
                ->setStatus($trackingEventDto->getStatus())
                ->setReason($trackingEventDto->getReason())
                ->setBranchName($trackingEventDto->getBranchName())
                ->setBranchAddress($trackingEventDto->getBranchAddress())
                ->setBranchComment($trackingEventDto->getBranchComment());

            $trackingInfo->addEvent($trackingEvent);
        }

        $trackingInfo->setLastEvent($trackingInfo->getEvents()->first());

        return $trackingInfo;
    }
}
