<?php

namespace App\Repository\Tracking;

use App\Model\Tracking\TrackingEvent;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method TrackingEvent|null find($id, $lockMode = null, $lockVersion = null)
 * @method TrackingEvent|null findOneBy(array $criteria, array $orderBy = null)
 * @method TrackingEvent[]    findAll()
 * @method TrackingEvent[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TrackingEventRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, TrackingEvent::class);
    }

    // /**
    //  * @return TrackingEvent[] Returns an array of TrackingEvent objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('t.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?TrackingEvent
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
