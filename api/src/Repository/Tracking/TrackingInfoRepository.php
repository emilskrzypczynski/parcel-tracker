<?php

namespace App\Repository\Tracking;

use App\Model\Tracking\TrackingInfo;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method TrackingInfo|null find($id, $lockMode = null, $lockVersion = null)
 * @method TrackingInfo|null findOneBy(array $criteria, array $orderBy = null)
 * @method TrackingInfo[]    findAll()
 * @method TrackingInfo[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TrackingInfoRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, TrackingInfo::class);
    }

    public function findOneByTrackingNumber(string $number): ?TrackingInfo
    {
        $qb = $this->createQueryBuilder('ti');

        return $qb->select('ti')
            ->addSelect('events')
            ->addSelect('last_event')
            ->leftJoin('ti.events', 'events')
            ->leftJoin('ti.lastEvent', 'last_event')
            ->where('ti.trackingNumber = :trackingNumber')
            ->orderBy('events.createdDate', 'desc')
            ->setParameter('trackingNumber', $number)
            ->getQuery()
            ->getOneOrNullResult();
    }

    public function save(TrackingInfo $trackingInfoEntity)
    {
        $this->_em->persist($trackingInfoEntity);
        $this->_em->flush();
    }
}
