<?php


namespace App\CommandHandler\Tracking;


use App\Command\Tracking\GetTrackingInfo;
use App\Factory\Tracking\TrackingInfoFactory;
use App\Model\Tracking\TrackingInfo;
use App\Repository\Tracking\TrackingInfoRepository;
use App\Utils\Tracking\Strategy\TrackerInterface;
use App\Utils\Tracking\Tracking;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

class GetTrackingInfoHandler implements MessageHandlerInterface
{
    private TrackingInfoRepository $trackingInfoRepository;
    private array $trackers;

    public function __construct(
        TrackingInfoRepository $trackingInfoRepository,
        array $trackers
    )
    {
        $this->trackingInfoRepository = $trackingInfoRepository;
        $this->trackers = $trackers;
    }

    public function __invoke(GetTrackingInfo $query): ?TrackingInfo
    {
        $existing = $this->trackingInfoRepository->findOneByTrackingNumber($query->getNumber());

        if ($existing && $existing->isFresh()) {
            return $existing;
        }

        $strategies = [];

        /** @var TrackerInterface $tracker */
        foreach ($this->trackers as $tracker) {
            if (in_array($tracker->getName(), $query->getCarriers())) {
                $strategies[] =  $tracker;
            }
        }

        $tracking = new Tracking($strategies);

        $trackingInfoDto = $tracking->getInfo($query->getNumber());

        if (null === $trackingInfoDto) {
            return null;
        }

        // Save result in database
        $trackingInfo = TrackingInfoFactory::fromDto($trackingInfoDto, $existing);
        $trackingInfo->setUpdatedAt(new \DateTime());

        $this->trackingInfoRepository->save($trackingInfo);

        return $trackingInfo;
    }
}
