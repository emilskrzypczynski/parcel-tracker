<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210618092526 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE tracking_events (id INT AUTO_INCREMENT NOT NULL, tracking_info_id INT DEFAULT NULL, created_at DATETIME NOT NULL, updated_at DATETIME DEFAULT NULL, name VARCHAR(190) NOT NULL, carrier VARCHAR(100) NOT NULL, created_date DATETIME DEFAULT NULL, code VARCHAR(100) DEFAULT NULL, status VARCHAR(100) DEFAULT NULL, reason VARCHAR(190) DEFAULT NULL, branch_name VARCHAR(190) DEFAULT NULL, branch_address VARCHAR(190) DEFAULT NULL, branch_comment LONGTEXT DEFAULT NULL, INDEX IDX_FB0BD406224940C0 (tracking_info_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE trackings (id INT AUTO_INCREMENT NOT NULL, last_event_id INT DEFAULT NULL, created_at DATETIME NOT NULL, updated_at DATETIME DEFAULT NULL, tracking_number VARCHAR(190) NOT NULL, success TINYINT(1) NOT NULL, origin_country VARCHAR(100) DEFAULT NULL, destination_country VARCHAR(100) DEFAULT NULL, status VARCHAR(100) DEFAULT NULL, status_desc VARCHAR(190) DEFAULT NULL, created_date DATETIME DEFAULT NULL, carrier VARCHAR(100) DEFAULT NULL, consolidation_tracking_number VARCHAR(100) DEFAULT NULL, shipping_time INT DEFAULT NULL, UNIQUE INDEX UNIQ_FA7EF26996AD15 (last_event_id), INDEX IDX_FA7EF263E1C9C18 (tracking_number), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE tracking_events ADD CONSTRAINT FK_FB0BD406224940C0 FOREIGN KEY (tracking_info_id) REFERENCES trackings (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE trackings ADD CONSTRAINT FK_FA7EF26996AD15 FOREIGN KEY (last_event_id) REFERENCES tracking_events (id) ON DELETE CASCADE');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE trackings DROP FOREIGN KEY FK_FA7EF26996AD15');
        $this->addSql('ALTER TABLE tracking_events DROP FOREIGN KEY FK_FB0BD406224940C0');
        $this->addSql('DROP TABLE tracking_events');
        $this->addSql('DROP TABLE trackings');
    }
}
